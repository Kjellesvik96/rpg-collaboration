package main.java.factories;
// Imports
import main.java.characters.supporters.Druid;
import main.java.characters.supporters.Priest;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.casters.Mage;
import main.java.characters.casters.Warlock;
import main.java.characters.meleers.Paladin;
import main.java.characters.meleers.Rogue;
import main.java.characters.meleers.Warrior;
import main.java.characters.rangers.Ranger;


/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Character as a return type when refactored to be good OO design.
*/
public class CharacterFactory {
    public Character getCharacter(CharacterType characterType) {
        switch(characterType) {
            case Mage:
                return new Mage();
            case Druid:
                return new Druid();
            case Rogue:
                return new Rogue();
            case Priest:
                return new Priest();
            case Ranger:
                return new Ranger();
            case Paladin:
                return new Paladin();
            case Warlock:
                return new Warlock();
            case Warrior:
                return new Warrior();
            default:
                return null;
        }
    }
}