package main.java.factories;

import main.java.spells.abstractions.Spell;
import main.java.spells.abstractions.SpellType;
import main.java.spells.damaging.ArcaneMissile;
import main.java.spells.damaging.ChaosBolt;
import main.java.spells.healing.Regrowth;
import main.java.spells.shielding.Barrier;

public class SpellFactory {
    public Spell getSpell(SpellType spellType) {
        switch(spellType) {
            case ArcaneMissile:
                return new ArcaneMissile();
            case Barrier:
                return new Barrier();
            case ChaosBolt:
                return new ChaosBolt();
            case Regrowth:
                return new Regrowth();
            default:
                return null;
        }
    }
}

