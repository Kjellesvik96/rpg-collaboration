package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.IRarity;

public class Legendary implements IRarity {
    // Stat modifier
    private double powerModifier = 2;
    // Color for display purposes
    private String itemRarityColor = Color.YELLOW;

    // Public properties
    @Override
    public double powerModifier() {
        return powerModifier;
    }

    @Override
    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
