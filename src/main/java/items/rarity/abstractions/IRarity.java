package main.java.items.rarity.abstractions;

public interface IRarity {

    public double powerModifier();

    public String getItemRarityColor();
}
