package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;

public class Cloth extends Armor {
    public Cloth(double itemRarity) {
        super(itemRarity,
                ArmorStatsModifiers.CLOTH_HEALTH_MODIFIER,
                ArmorStatsModifiers.CLOTH_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.CLOTH_MAGIC_RED_MODIFIER);
    }
}
