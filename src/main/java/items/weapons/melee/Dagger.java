package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Dagger extends Weapon {
    public Dagger() {
        super();
    }

    public Dagger(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.DAGGER_ATTACK_MOD);
    }
}
