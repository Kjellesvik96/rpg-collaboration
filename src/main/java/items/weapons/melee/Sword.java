package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Sword extends Weapon {
    public Sword() {
        super();
    }

    public Sword(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.SWORD_ATTACK_MOD);
    }
}
