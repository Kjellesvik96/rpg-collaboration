package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Axe extends Weapon {
    public Axe() {
        super();
    }

    public Axe(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.AXE_ATTACK_MOD);
    }
}
