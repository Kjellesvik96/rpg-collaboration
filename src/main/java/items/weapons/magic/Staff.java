package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Staff extends Weapon {
    public Staff() {
        super();
    }

    public Staff(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.STAFF_MAGIC_MOD);
    }
}