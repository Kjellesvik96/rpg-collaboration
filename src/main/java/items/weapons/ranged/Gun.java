package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Gun extends Weapon {
    public Gun() {
        super();
    }

    public Gun(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.GUN_ATTACK_MOD);
    }
}
