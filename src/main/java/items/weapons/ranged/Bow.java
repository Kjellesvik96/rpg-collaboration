package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Bow extends Weapon {
    public Bow() {
        super();
    }

    public Bow(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.BOW_ATTACK_MOD);
    }
}
