package main.java.items.weapons.blunt;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.weapons.abstractions.Weapon;

public class Mace extends Weapon {
    public Mace() {
        super();
    }

    public Mace(ItemRarity rarity) {
        super(rarity, WeaponStatsModifiers.MACE_ATTACK_MOD);
    }
}
