package main.java.items.weapons.abstractions;

import main.java.items.rarity.abstractions.ItemRarity;

public abstract class Weapon {

    // Variables
    protected ItemRarity rarity;
    protected double attackPowerModifier;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }
    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }

    // Constructors
    public Weapon() {
        this.rarity = ItemRarity.Common;
    }

    public Weapon(ItemRarity rarity, double attackPowerModifier) {
        this.rarity = rarity;
        this.attackPowerModifier = attackPowerModifier;
    }
}

