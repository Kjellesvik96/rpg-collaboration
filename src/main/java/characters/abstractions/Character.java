package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;

public abstract class Character implements ICharacter {

    // Active trackers and flags
    public Armor armor;
    public Weapon weapon;
    public double currentHealth;
    public Boolean isDead = false;

    // Public getters statuses and stats
    public double getCurrentHealth() {
        return currentHealth;
    }
    public Boolean getDead() {
        return isDead;
    }


    // Equipment behaviours
    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    @Override
    public void equipArmor(Armor armor) {
        this.armor  = armor;
    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    @Override
    public void equipWeapon(Weapon weapon) {
        this.weapon = weapon;
    }


    // Character behaviours
    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @param damageType
     */
    @Override
    public double takeDamage(double incomingDamage, String damageType) {
        /*double damageTaken = 0;
        if(damageType.equals("Physical")) {
            damageTaken += incomingDamage * (1 - (this.basePhysReductionPercent
                    * this.armor.getPhysRedModifier() * this.armor.getRarityModifier()));
        }
        if(damageType.equals("Magical")) {
            damageTaken += incomingDamage * (1 - (this.baseMagicReductionPercent
                    * this.armor.getMagicRedModifier() * this.armor.getRarityModifier()));
        }
        return damageTaken; */
        return 0; // Return damage taken after damage reductions, based on type of damage.
    }
}
