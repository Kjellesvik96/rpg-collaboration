package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;

public interface ICharacter {
    void equipArmor(Armor armor);
    void equipWeapon(Weapon weapon);
    double takeDamage(double incomingDamage, String damageType);

}
