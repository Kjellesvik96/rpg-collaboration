package main.java.characters.abstractions.categories;

import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.ShieldingSpell;

public abstract class Support extends Character {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Support;
    public final ArmorType ARMOR_TYPE = ArmorType.Cloth;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;
    public ShieldingSpell shieldingSpell;
    public HealingSpell healingSpell;

    /**
     * Constructs a newly allocated {@code Character} object that
     * represents the specified {@code char} value.
     *
     * @param value the value to be represented by the
     *              {@code Character} object.
     * @deprecated It is rarely appropriate to use this constructor. The static factory
     * {@link #valueOf(char)} is generally a better choice, as it is
     * likely to yield significantly better space and time performance.
     */
    public Support(char value) {
        super(value);
    }

    public void equipSpell1(ShieldingSpell shieldingSpell){
        this.shieldingSpell = shieldingSpell;
    }

    public void equipSpell2(HealingSpell healingSpell){
        this.healingSpell = healingSpell;
    }

    public double healPartyMember() {
        //return this.healingSpell.getHealingAmount() * this.weapon.getWeaponModifier()
                //* this.weapon.getRarityModifier();
        //return healingDone; // Return healing amount based on spell and modifiers.
        return 0.0;
    }
}