package main.java.characters.abstractions.categories;

import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;

public abstract class Melee extends Character{
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Melee;
}
