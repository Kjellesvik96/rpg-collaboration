package main.java.characters.abstractions.categories;

import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;

public abstract class Ranged extends Character{
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Ranged;
    public final ArmorType ARMOR_TYPE = ArmorType.Mail;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Ranged;

    /**
     * Constructs a newly allocated {@code Character} object that
     * represents the specified {@code char} value.
     *
     * @param value the value to be represented by the
     *              {@code Character} object.
     * @deprecated It is rarely appropriate to use this constructor. The static factory
     * {@link #valueOf(char)} is generally a better choice, as it is
     * likely to yield significantly better space and time performance.
     */
    public Ranged(char value) {
        super(value);
    }
}
