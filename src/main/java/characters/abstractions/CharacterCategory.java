package main.java.characters.abstractions;

/*
 This enumerator exists to server as a placeholder for a character type.
 This should be replaced by some class hierarchy with these types as parent classes
 to the hero classes. There should be some base Hero class as well, that these Type subclasses
 inherit from.
*/
public enum CharacterCategory {
    Caster,
    Ranged,
    Melee,
    Support
}
