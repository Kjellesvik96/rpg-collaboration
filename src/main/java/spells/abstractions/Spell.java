package main.java.spells.abstractions;

public interface Spell {
    String getSpellName();
}
