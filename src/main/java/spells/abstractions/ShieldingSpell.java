package main.java.spells.abstractions;

public interface ShieldingSpell extends Spell {
    double getAbsorbShieldPercentage();
}
